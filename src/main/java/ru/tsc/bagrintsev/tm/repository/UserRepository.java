package ru.tsc.bagrintsev.tm.repository;

import ru.tsc.bagrintsev.tm.api.repository.IUserRepository;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectParameterNameException;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.bagrintsev.tm.util.HashUtil;

import java.security.GeneralSecurityException;
import java.util.*;

public class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public User create(final String login, final String password) throws GeneralSecurityException {
        final User user = new User();
        user.setLogin(login);
        setUserPassword(user, password);
        return add(user);
    }
    @Override
    public User setParameter(final User user,
                             final EntityField paramName,
                             final String paramValue) throws IncorrectParameterNameException {
        switch (paramName) {
            case EMAIL:
                user.setEmail(paramValue);
                break;
            case FIRST_NAME:
                user.setFirstName(paramValue);
                break;
            case MIDDLE_NAME:
                user.setMiddleName(paramValue);
                break;
            case LAST_NAME:
                user.setLastName(paramValue);
                break;
            default: throw new IncorrectParameterNameException(paramName, "User");
        }
        return user;
    }

    @Override
    public User setRole(final User user, final Role role) {
        user.setRole(role);
        return user;
    }

    @Override
    public void setUserPassword(final User user, final String password) throws GeneralSecurityException {
        byte[] salt = HashUtil.generateSalt();
        user.setPasswordSalt(salt);
        user.setPasswordHash(HashUtil.generateHash(password, salt));
    }

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User findById(final String id) throws UserNotFoundException {
        for (final User user : users) {
            if (id.equals(user.getId())) {
                return user;
            }
        }
        throw new UserNotFoundException("Id", id);
    }

    @Override
    public User findByLogin(final String login) throws UserNotFoundException {
        for (final User user : users) {
            if (login.equals(user.getLogin())) {
                return user;
            }
        }
        throw new UserNotFoundException("Login", login);
    }

    @Override
    public User findByEmail(final String email) throws UserNotFoundException {
        for (final User user : users) {
            if (email.equals(user.getEmail())) {
                return user;
            }
        }
        throw new UserNotFoundException("Email", email);
    }

    @Override
    public User remove(final User user) {
        users.remove(user);
        return user;
    }

    @Override
    public User removeById(final String id) throws UserNotFoundException {
        final User user = findById(id);
        return remove(user);
    }

    @Override
    public User removeByLogin(final String login) throws UserNotFoundException {
        final User user = findByLogin(login);
        return remove(user);
    }

}
