package ru.tsc.bagrintsev.tm.service;

import ru.tsc.bagrintsev.tm.api.repository.IProjectRepository;
import ru.tsc.bagrintsev.tm.api.repository.ITaskRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IProjectTaskService;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.Map;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository,
                              final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String projectId, final String taskId) throws AbstractException {
        if (projectId == null || projectId.isEmpty()) throw new IdIsEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdIsEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existsById(taskId)) throw new TaskNotFoundException();
        taskRepository.setProjectId(taskId, projectId);
    }

    @Override
    public void unbindTaskFromProject(final String projectId, final String taskId) throws AbstractException {
        if (projectId == null || projectId.isEmpty()) throw new IdIsEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdIsEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existsById(taskId)) throw new TaskNotFoundException();
        taskRepository.setProjectId(taskId, null);
    }

    @Override
    public void removeProjectById(final String projectId) throws AbstractException {
        if (projectId == null || projectId.isEmpty()) throw new IdIsEmptyException();
        final Map<Integer, Task> tasks = taskRepository.findAllByProjectId(projectId);
        for (final Map.Entry<Integer, Task> taskEntry : tasks.entrySet()) {
            final Task task = taskEntry.getValue();
            taskRepository.removeById(task.getId());
        }
        projectRepository.removeById(projectId);
    }

}
