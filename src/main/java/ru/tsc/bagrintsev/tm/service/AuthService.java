package ru.tsc.bagrintsev.tm.service;

import ru.tsc.bagrintsev.tm.api.sevice.IAuthService;
import ru.tsc.bagrintsev.tm.api.sevice.IUserService;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.AccessDeniedException;
import ru.tsc.bagrintsev.tm.exception.field.LoginIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.PasswordIsEmptyException;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.bagrintsev.tm.util.HashUtil;

import java.security.GeneralSecurityException;

public class AuthService implements IAuthService {

    private final IUserService userService;

    public String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void signIn(final String login, final String password) throws AbstractException, GeneralSecurityException {
        if (login == null || login.isEmpty()) throw new LoginIsEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordIsEmptyException();
        final User user = userService.findByLogin(login);
        if (!user.getPasswordHash().equals(HashUtil.generateHash(password, user.getPasswordSalt()))) {
            throw new AccessDeniedException();
        }
        userId = user.getId();
    }

    @Override
    public void signOut() {
        userId = null;
    }

    @Override
    public String getCurrentUserId() {
        return userId;
    }

    @Override
    public User getCurrentUser() throws AbstractException {
        return userService.findById(userId);
    }
}
