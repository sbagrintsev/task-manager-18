package ru.tsc.bagrintsev.tm.service;

import ru.tsc.bagrintsev.tm.api.repository.IUserRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IUserService;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.*;
import ru.tsc.bagrintsev.tm.model.User;

import java.security.GeneralSecurityException;
import java.util.List;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginIsEmptyException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User findByEmail(final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailIsEmptyException();
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean isLoginExists(final String login) {
        if (login == null || login.isEmpty()) return false;
        try {
            findByLogin(login);
        } catch (AbstractException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isEmailExists(final String email) {
        if (email == null || email.isEmpty()) return false;
        try {
            findByEmail(email);
        } catch (AbstractException e) {
            return false;
        }
        return true;
    }

    @Override
    public User removeUser(final User user) throws UserNotFoundException {
        if (user == null) throw new UserNotFoundException();
        return userRepository.remove(user);
    }

    @Override
    public User removeById(final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        return userRepository.removeById(id);
    }

    @Override
    public User removeByLogin(final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginIsEmptyException();
        return userRepository.removeByLogin(login);
    }

    @Override
    public User create(final String login, final String password) throws GeneralSecurityException, AbstractFieldException {
        if (login == null || login.isEmpty()) throw new LoginIsEmptyException();
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        if (password == null || password.isEmpty()) throw new PasswordIsEmptyException();
        return userRepository.create(login, password);
    }

    @Override
    public User setParameter(final User user,
                             final EntityField paramName,
                             final String paramValue) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        if (EntityField.EMAIL.equals(paramName) && isEmailExists(paramValue)) throw new EmailAlreadyExistsException(paramValue);
        return userRepository.setParameter(user, paramName, paramValue);
    }

    @Override
    public User setRole(final User user, final Role role) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        if (role == null) throw new IncorrectRoleException();
        return userRepository.setRole(user, role);
    }

    @Override
    public User setPassword(final String userId, final String password) throws AbstractException, GeneralSecurityException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (password == null || password.isEmpty()) throw new PasswordIsEmptyException();
        final User user = findById(userId);
        userRepository.setUserPassword(user, password);
        return user;
    }

    @Override
    public User updateUser(
            final String userId,
            final String firstName,
            final String lastName,
            final String middleName
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        final User user = findById(userId);
        setParameter(user, EntityField.FIRST_NAME, firstName);
        setParameter(user, EntityField.MIDDLE_NAME, middleName);
        setParameter(user, EntityField.LAST_NAME, lastName);
        return user;
    }

}
