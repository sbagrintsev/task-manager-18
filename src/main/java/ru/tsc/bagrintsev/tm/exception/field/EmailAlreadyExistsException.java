package ru.tsc.bagrintsev.tm.exception.field;

public class EmailAlreadyExistsException extends AbstractFieldException{

    public EmailAlreadyExistsException(final String email) {
        super(String.format("Error! Email %s is already in use...", email));
    }

}
