package ru.tsc.bagrintsev.tm.exception.field;

public class IncorrectRoleException extends AbstractFieldException{

    public IncorrectRoleException() {
        super("Error! Role is incorrect...");
    }

}
