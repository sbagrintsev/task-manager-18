package ru.tsc.bagrintsev.tm.exception.field;

import ru.tsc.bagrintsev.tm.enumerated.EntityField;

public class IncorrectParameterNameException extends AbstractFieldException{

    public IncorrectParameterNameException(final EntityField paramName, final String entityName) {
        super(String.format("Error! There is no parameter: %s on entity: %s...", paramName, entityName));
    }

}
