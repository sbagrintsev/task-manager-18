package ru.tsc.bagrintsev.tm.exception.field;

public final class LoginIsEmptyException extends AbstractFieldException{

    public LoginIsEmptyException() {
        super("Error! Login is empty...");
    }

}
