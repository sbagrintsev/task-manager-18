package ru.tsc.bagrintsev.tm.command.user;

import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

public class UserUpdateProfileCommand extends AbstractUserCommand{
    @Override
    public void execute() throws IOException, AbstractException {
        showOperationInfo();
        showParameterInfo(EntityField.LOGIN);
        final String login = TerminalUtil.nextLine();
        final String userId = getUserService().findByLogin(login).getId();
        showParameterInfo(EntityField.FIRST_NAME);
        final String firstName = TerminalUtil.nextLine();
        showParameterInfo(EntityField.MIDDLE_NAME);
        final String middleName = TerminalUtil.nextLine();
        showParameterInfo(EntityField.LAST_NAME);
        final String lastName = TerminalUtil.nextLine();
        getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    public String getName() {
        return "user-update";
    }

    @Override
    public String getDescription() {
        return "Update user profile.";
    }

}
