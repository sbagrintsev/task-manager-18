package ru.tsc.bagrintsev.tm.command.project;

import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws IOException {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT TYPE: ");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortValue = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortValue);
        if (sort == null) {
            System.out.println("Sort type is wrong, applied default sort");
        }
        final List<Project> projects = getProjectService().findAll(sort);
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(project);
        }
    }

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Print project list.";
    }

}
