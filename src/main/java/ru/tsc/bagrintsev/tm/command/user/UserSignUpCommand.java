package ru.tsc.bagrintsev.tm.command.user;

import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;

public class UserSignUpCommand extends AbstractUserCommand{
    @Override
    public void execute() throws IOException, AbstractException, GeneralSecurityException {
        showOperationInfo();
        showParameterInfo(EntityField.LOGIN);
        final String login = TerminalUtil.nextLine();
        showParameterInfo(EntityField.PASSWORD);
        final String password = TerminalUtil.nextLine();
        showParameterInfo(EntityField.ROLE);
        System.out.println(Arrays.toString(Role.values()));
        final Role role = Role.toRole(TerminalUtil.nextLine());
        showParameterInfo(EntityField.EMAIL);
        final String email = TerminalUtil.nextLine();
        final User user = getUserService().create(login, password);
        getUserService().setParameter(user, EntityField.EMAIL, email);
        getUserService().setRole(user, role);
    }

    @Override
    public String getName() {
        return "user-sign-up";
    }

    @Override
    public String getDescription() {
        return "Sign-up new user in system.";
    }

}
