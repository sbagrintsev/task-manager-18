package ru.tsc.bagrintsev.tm.command;

import ru.tsc.bagrintsev.tm.api.model.ICommand;
import ru.tsc.bagrintsev.tm.api.sevice.IServiceLocator;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

import java.io.IOException;
import java.security.GeneralSecurityException;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract void execute() throws IOException, AbstractException, GeneralSecurityException;

    public abstract String getName();

    public abstract String getShortName();

    public abstract String getDescription();

    protected void showOperationInfo() {
        System.out.printf("[%s]%n", getName());
    }

    protected void showParameterInfo(final EntityField parameter) {
        System.out.printf("Enter %s: %n", parameter.getDisplayName());
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("[");
        result.append(String.format("%-35s", getName()));
        result.append(" | ");
        result.append(String.format("%-25s", getShortName()));
        result.append("]");
        if (!getDescription().isEmpty()) {
            while (result.length() < 75) {
                result.append(" ");
            }
            result.append(getDescription());
        }
        return result.toString();
    }

}
