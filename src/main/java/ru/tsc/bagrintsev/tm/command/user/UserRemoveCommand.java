package ru.tsc.bagrintsev.tm.command.user;

import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

public class UserRemoveCommand extends AbstractUserCommand{
    @Override
    public void execute() throws IOException, AbstractException {
        showOperationInfo();
        showParameterInfo(EntityField.ID);
        final String userId = TerminalUtil.nextLine();
        getUserService().removeById(userId);
    }

    @Override
    public String getName() {
        return "user-remove";
    }

    @Override
    public String getDescription() {
        return "Remove user from system.";
    }

}
