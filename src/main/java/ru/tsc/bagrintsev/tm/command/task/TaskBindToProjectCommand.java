package ru.tsc.bagrintsev.tm.command.task;

import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

public class TaskBindToProjectCommand extends AbstractTaskCommand {
    @Override
    public void execute() throws AbstractException, IOException {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.print("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        System.out.print("ENTER TASK ID: ");
        final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().bindTaskToProject(projectId, taskId);
    }

    @Override
    public String getName() {
        return "task-bind-to-project";
    }

    @Override
    public String getDescription() {
        return "Bind task to project.";
    }
}
