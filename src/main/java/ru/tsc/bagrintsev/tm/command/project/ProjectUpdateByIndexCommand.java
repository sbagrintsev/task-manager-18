package ru.tsc.bagrintsev.tm.command.project;

import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {
    @Override
    public void execute() throws IOException, AbstractException {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        getProjectService().updateByIndex(index, name, description);
    }

    @Override
    public String getName() {
        return "project-update-by-index";
    }

    @Override
    public String getDescription() {
        return "Update project by index.";
    }
}
