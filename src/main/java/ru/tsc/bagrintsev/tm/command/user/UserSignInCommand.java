package ru.tsc.bagrintsev.tm.command.user;

import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class UserSignInCommand extends AbstractUserCommand{
    @Override
    public void execute() throws IOException, AbstractException, GeneralSecurityException {
        showOperationInfo();
        showParameterInfo(EntityField.LOGIN);
        final String login = TerminalUtil.nextLine();
        showParameterInfo(EntityField.PASSWORD);
        final String password = TerminalUtil.nextLine();
        getAuthService().signIn(login, password);
    }

    @Override
    public String getName() {
        return "user-sign-in";
    }

    @Override
    public String getDescription() {
        return "Sign user in.";
    }

}
