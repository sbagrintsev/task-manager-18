package ru.tsc.bagrintsev.tm.command.system;

import ru.tsc.bagrintsev.tm.api.model.ICommand;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandsCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[Interaction commands]");
        Collection<AbstractCommand> repository = getCommandService().getAvailableCommands();
        for (ICommand command : repository) {
            String commandName = command.getName();
            String description = command.getDescription();
            if (commandName == null || commandName.isEmpty()) continue;
            System.out.printf("%-35s%s\n", commandName, description);
        }
    }

    @Override
    public String getName() {
        return "commands";
    }

    @Override
    public String getShortName() {
        return "-cmd";
    }

    @Override
    public String getDescription() {
        return "Print application interaction commands.";
    }

}
