package ru.tsc.bagrintsev.tm.command.project;

import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

public class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws IOException, AbstractException {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusById(id, Status.COMPLETED);
    }

    @Override
    public String getName() {
        return "project-complete-by-id";
    }

    @Override
    public String getDescription() {
        return "Complete project by id.";
    }

}
