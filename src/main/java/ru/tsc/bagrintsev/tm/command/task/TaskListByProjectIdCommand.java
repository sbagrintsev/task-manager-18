package ru.tsc.bagrintsev.tm.command.task;

import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;
import java.util.Map;

public class TaskListByProjectIdCommand extends AbstractTaskCommand {
    @Override
    public void execute() throws IdIsEmptyException, IOException {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.print("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        final Map<Integer, Task> tasks = getTaskService().findAllByProjectId(projectId);
        for (final Map.Entry<Integer, Task> taskEntry : tasks.entrySet()) {
            final int index = taskEntry.getKey() + 1;
            final Task task = taskEntry.getValue();
            if (task == null) continue;
            System.out.println(index + ". " + task);
        }
    }

    @Override
    public String getName() {
        return "task-list-by-project-id";
    }

    @Override
    public String getDescription() {
        return "List tasks by project id.";
    }
}
