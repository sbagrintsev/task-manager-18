package ru.tsc.bagrintsev.tm.command.project;

import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

public class ProjectStartByIdCommand extends AbstractProjectCommand {
    @Override
    public void execute() throws IOException, AbstractException {
        System.out.println("[START PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public String getName() {
        return "project-start-by-id";
    }

    @Override
    public String getDescription() {
        return "Start project by id.";
    }
}
