package ru.tsc.bagrintsev.tm.enumerated;

import ru.tsc.bagrintsev.tm.exception.field.IncorrectRoleException;

public enum Role {

    REGULAR("Regular user"),
    ADMIN("Administrator");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public static Role toRole(final String value) throws IncorrectRoleException {
        if (value == null || value.isEmpty()) throw new IncorrectRoleException();
        for (Role role : Role.values()) {
            if (role.toString().equals(value)) {
                return role;
            }
        }
        throw new IncorrectRoleException();
    }

}
