package ru.tsc.bagrintsev.tm.enumerated;

public enum EntityField {
    ID("id"),
    LOGIN("login"),
    PASSWORD("password"),
    EMAIL("email"),
    FIRST_NAME("first name"),
    MIDDLE_NAME("middle name"),
    LAST_NAME("last name"),
    ROLE("role");

    private final String displayName;

    EntityField(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
