package ru.tsc.bagrintsev.tm.api.repository;

import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

public interface ITaskRepository {

    Task create(String name);

    Task create(String name, String description);

    Task add(Task task);

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    Map<Integer, Task> findAllByProjectId(String projectId);

    Task findOneByIndex(Integer index);

    Task findOneById(String id);

    Task remove(Task task);

    Task removeByIndex(Integer index) throws TaskNotFoundException;

    Task removeById(String id) throws TaskNotFoundException;

    int taskCount();

    boolean existsById(String id);

    void setProjectId(String taskId, String projectId) throws TaskNotFoundException;

    void clear();

}
