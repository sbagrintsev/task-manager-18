package ru.tsc.bagrintsev.tm.api.repository;

import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    Project create(String name);

    Project create(String name, String description);

    Project add(Project project);

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    Project findOneByIndex(Integer index);

    Project findOneById(String id) throws ProjectNotFoundException;

    boolean existsById(String id) throws ProjectNotFoundException;

    Project remove(Project project);

    Project removeByIndex(Integer index) throws ProjectNotFoundException;

    Project removeById(String id) throws ProjectNotFoundException;

    int projectCount();

    void clear();
}
