package ru.tsc.bagrintsev.tm.api.repository;

import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectParameterNameException;
import ru.tsc.bagrintsev.tm.model.User;

import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

public interface IUserRepository {

    User create(String login, String password) throws GeneralSecurityException;

    User setParameter(User user,
                      EntityField paramName,
                      String paramValue) throws IncorrectParameterNameException;

    User setRole(User user, Role role);

    void setUserPassword(User user, String password) throws GeneralSecurityException;

    User add(User user);

    List<User> findAll();

    User findById(String id) throws UserNotFoundException;

    User findByLogin(String login) throws UserNotFoundException;

    User findByEmail(String email) throws UserNotFoundException;

    User remove(User user);

    User removeById(String id) throws UserNotFoundException;

    User removeByLogin(String login) throws UserNotFoundException;
}
