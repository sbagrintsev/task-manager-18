package ru.tsc.bagrintsev.tm.api.sevice;

import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.User;

import java.security.GeneralSecurityException;

public interface IAuthService {
    void signIn(String login, String password) throws AbstractException, GeneralSecurityException;

    void signOut();

    String getCurrentUserId();

    User getCurrentUser() throws AbstractException;
}
