package ru.tsc.bagrintsev.tm.api.sevice;

import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.AbstractFieldException;
import ru.tsc.bagrintsev.tm.model.User;

import java.security.GeneralSecurityException;
import java.util.List;

public interface IUserService {
    List<User> findAll();

    User findById(String id) throws AbstractException;

    User findByLogin(String login) throws AbstractException;

    User findByEmail(String email) throws AbstractException;

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    User removeUser(User user) throws UserNotFoundException;

    User removeById(String id) throws AbstractException;

    User removeByLogin(String login) throws AbstractException;

    User create(String login, String password) throws GeneralSecurityException, AbstractFieldException;

    User setParameter(User user,
                      EntityField paramName,
                      String paramValue) throws AbstractException;

    User setRole(User user, Role role) throws AbstractException;

    User setPassword(String userId, String password) throws AbstractException, GeneralSecurityException;

    User updateUser(
            String userId,
            String firstName,
            String lastName,
            String middleName
    ) throws AbstractException;
}
